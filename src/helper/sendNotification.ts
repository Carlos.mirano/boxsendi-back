// amc
import axios from 'axios';
import { Request, Response } from 'express';

const sendNotification = async (req: Request, res: Response) => {

  let db = (<any>req).db;

  let messageStateQuotation: String = '';

  switch ( db.estado ) {
    case '1':
      messageStateQuotation = 'Su producto está pendiente de revisión';
      break;
    case '2':
      messageStateQuotation = 'Su producto está en almacén origen';
      break;
    case '3':
      messageStateQuotation = 'Su producto ya está en viaje';
      break;
    case '4':
      messageStateQuotation = 'Su producto está en almacén destino';
      break;
    case '5':
      messageStateQuotation = 'Su producto fue cancelado';
      break;
    case '6':
      messageStateQuotation = 'Su producto fue entregado';
      break;
    default:
      messageStateQuotation = '-';
  }

  const data = {
    to: process.env.DISPO,
    notification: {
      title: "BoxSendi App",
      body: messageStateQuotation
    }
  };

  try {

    await axios.post('https://fcm.googleapis.com/fcm/send', data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': process.env.KEY_FCM
      }
    }).then( () => {
      return res.status(200).json({
        db,
        msg: 'Actualización Exitosa',
        msgNotification: 'Se envió la notificación',
      });
    }).catch( (error) => {
      return res.status(404).json({
        error,
        msg: 'Error en el envío',
      });
    });

  } catch (error) {
    return res.status(500).json({
      msg: 'Error POST Notification',
    });
  }

}

export default sendNotification;
