import jwt from 'jsonwebtoken';

const generateJWT = (id: any) => {
  return new Promise((resolve, reject) => {
    const payload = { id };

    jwt.sign(
      payload,
      process.env.KEY_JWT as string,
      { expiresIn: 60 * 600 },
      (error, token) => {
        if (error) {
          reject('Error al generar el token');
        } else {
          resolve(token);
        }
      }
    );
  });
};

export default generateJWT;
