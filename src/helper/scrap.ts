import puppeteer from 'puppeteer';

const scrap = async (url: string) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setDefaultNavigationTimeout(0);
  await page.setViewport({ width: 1280, height: 800 });
  await page.goto(url, { waitUntil: 'networkidle2' });
  await page.waitForSelector('#dp-container');
  var article;

  try {
    const existePrecio = await page.$$(
      "span[class='a-size-medium a-color-price priceBlockBuyingPriceString']"
    );

    if (existePrecio.length > 0) {
      article = await page.evaluate(() => {
        const item = {
          descripcion: '',
          moneda: '',
          precio: '',
          imagen: '',
          asin: '',
        };

        const asin = (
          document.querySelector(
            "#productDetails_db_sections td[class='a-size-base prodDetAttrValue']"
          ) as HTMLTableCaptionElement
        ).innerText;
        const descripcion = (
          document.querySelector(
            "span[class='a-size-large product-title-word-break']"
          ) as HTMLSpanElement
        ).innerText;
        const precio = (
          document.querySelector(
            "span[class='a-size-medium a-color-price priceBlockBuyingPriceString']"
          ) as HTMLSpanElement
        ).innerText;
        const imagen = (
          document.querySelector('div.imgTagWrapper img') as HTMLImageElement
        ).src;
        item.descripcion = descripcion;
        let p = precio.replace(/\u00a0/g, ' ');
        item.moneda = p.split(' ')[0];
        item.precio = p.split(' ')[1].replace(',', '');
        item.imagen = imagen;
        item.asin = asin;
        return item;
      });
    }
  } catch (error) {
    console.log('Ocurrio un error', error);
  }

  await browser.close();
  return article;
};

export default scrap;
