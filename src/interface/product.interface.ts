export interface IProducto {
  asin: string;
  imagen: string;
  descripcion: string;
  moneda: string;
  precio: number;
}
