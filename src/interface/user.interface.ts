export interface IUser {
  name: string;
  user: string;
  mail: string;
  password: string;
  role: string;
  admin: boolean;
}
