export interface IQuotation {
  usuario: string;
  asin: string;
  cantidad: string;
  unidad: string;
  peso: string;
  paquete: string;
  categoria: string;
  precioTotal: string;
  flete: string;
  arancel: string;
  subTotal: string;
  descuento: string;
  impuesto: string;
  total: string;
  origen: string;
  destino: string;
  estado: string;
}
