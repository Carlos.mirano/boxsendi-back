export interface IMenu {
  admin: boolean;
  icon: string;
  title: string;
  to: string;
}
