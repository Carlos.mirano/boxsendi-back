/**
 * Ruta: /api/login
 */

import { Router } from 'express';
import { check } from 'express-validator';

import login from '../controller/auth.controller';
import validarCampos from '../middleware/validarCampos.middleware';

const router = Router();

router.post(
  '/',
  [
    check('mail', 'Digite el correo electrónico').isEmail(),
    check('password', 'Digite la contraseña').not().isEmpty(),
    validarCampos,
  ],
  login
);

export default router;
