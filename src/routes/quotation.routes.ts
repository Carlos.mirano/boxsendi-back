/**
 * Ruta: /api/cotizaciones
 */
import { Router } from 'express';
import {
  createQuotation,
  getQuotations,
  updateQuotationController,
  deleteQuotationController,
  getAllQuotationController
} from '../controller/quotation.controller';
import sendNotification from '../helper/sendNotification';
import validarJWT from '../middleware/validarJWT.middleware';

const router = Router();

router.get('/:id', validarJWT, getQuotations);
router.get('/', validarJWT, getAllQuotationController);
router.post('/', validarJWT, createQuotation);
router.put('/:id', validarJWT, updateQuotationController, sendNotification);
router.delete('/:id/:estado', validarJWT, deleteQuotationController)

export default router;
