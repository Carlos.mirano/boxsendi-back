/**
 * Ruta: /api/orderstate
 */
import { Router } from 'express';
import {
  listOrderStateController,
  createOrderStateController,
  updateOrderStateController,
  deleteOrderStateController,
} from '../controller/orderState.controller';
import validarJWT from '../middleware/validarJWT.middleware';

const router = Router();

router.get('/', validarJWT, listOrderStateController);
router.post('/', validarJWT, createOrderStateController);
router.put('/:id', validarJWT, updateOrderStateController);
router.delete('/:id', validarJWT, deleteOrderStateController);

export default router;
