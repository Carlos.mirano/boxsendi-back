/**
 * Ruta: /api/usuarios
 */
import { Router } from 'express';
import { check } from 'express-validator';

import { createUser, getAllUser, getUser } from '../controller/user.controller';

import validarCampos from '../middleware/validarCampos.middleware';
import validarJWT from '../middleware/validarJWT.middleware';

const router = Router();

router.get('/', validarJWT, getAllUser);

router.get('/:id', validarJWT, getUser);

router.post(
  '/',
  [
    check('name', 'Digite el nombre').not().isEmpty(),
    check('user', 'Digite el usuario').not().isEmpty(),
    check('mail', 'Digite el correo electrónico').isEmail(),
    check('password', 'Digite la contraseña').not().isEmpty(),
    validarCampos,
  ],
  createUser
);

export default router;
