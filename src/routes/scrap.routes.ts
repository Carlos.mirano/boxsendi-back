/**
 * Ruta: /api/scraping
 */
import { Router } from 'express';
import { scraping } from '../controller/scrap.controller';

const router = Router();

router.post('/', scraping);

export default router;
