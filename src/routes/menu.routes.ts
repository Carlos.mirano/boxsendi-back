/**
 * Ruta: /api/menu
 */
import { Router } from 'express';
import {
  createMenuController,
  deleteMenuController,
  listMenuController,
  updateMenuController,
} from '../controller/menu.controller';
import validarJWT from '../middleware/validarJWT.middleware';

const router = Router();

router.get('/:id', validarJWT, listMenuController);
router.post('/', validarJWT, createMenuController);
router.put('/:id', validarJWT, updateMenuController);
router.delete('/:id', validarJWT, deleteMenuController);

export default router;
