/**
 * Ruta: /api/productos
 */
import { Router } from 'express';
import validarJWT from '../middleware/validarJWT.middleware';
import { getAllProducts, getProduct } from '../controller/product.controller';

const router = Router();

router.get('/', validarJWT, getAllProducts);
router.get('/:id', validarJWT,getProduct);

export default router;
