/**
 * Ruta: /api/categoria
 */
import { Router } from 'express';
import {
  createCategoriaController,
  deleteCategoriaController,
  listCategoriaController,
  updateCategoriaController,
} from '../controller/categoria.controller';
import validarJWT from '../middleware/validarCampos.middleware';

const router = Router();

router.get('/', validarJWT, listCategoriaController);
router.post('/', validarJWT, createCategoriaController);
router.put('/', validarJWT, updateCategoriaController);
router.delete('/', validarJWT, deleteCategoriaController);

export default router;
