import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';

const validarJWT = (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers.authorization?.split(' ')[1];

  if (!token) {
    return res.status(401).json({
      ok: false,
      msg: 'No hay token',
    });
  }

  try {
    const { id } = <any>verify(token, process.env.KEY_JWT as string);
    (<any>req).idUser = id;
    next();
  } catch (error) {
    return res.status(401).json({
      ok: false,
      msg: 'Token no válido',
    });
  }
};

export default validarJWT;
