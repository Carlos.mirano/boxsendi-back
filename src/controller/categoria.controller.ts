import { Request, Response } from 'express';
import { ICategoria } from '../interface/categoria.interface';
import {
  listCategoriaService,
  createCategoriaService,
  updateCategoriaService,
  deleteCategoriaService,
} from '../service/categoria.service';

const listCategoriaController = async (req: Request, res: Response) => {
  const { categorias } = await listCategoriaService();
  res.status(200).json({
    categorias,
  });
};

const createCategoriaController = async (req: Request, res: Response) => {
  const categoria: ICategoria = <any>req.body;
  try {
    const db = await createCategoriaService(categoria);
    res.status(200).json({
      db,
      msg: 'Registro exitoso.',
    });
  } catch (error) {
    res.status(500).json({
      error,
      msg: 'Ocurrió un error',
    });
  }
};

const updateCategoriaController = async (req: Request, res: Response) => {
  const categoria: ICategoria = <any>req.body;
  await updateCategoriaService(categoria);
  res.status(200).json({
    msg: 'Categoria actualizada correctamente',
  });
};

const deleteCategoriaController = async (req: Request, res: Response) => {
  const categoria: ICategoria = <any>req.body;
  await deleteCategoriaService(categoria);
  res.status(200).json({
    msg: 'Categoria eliminada correctamente',
  });
};

export {
  listCategoriaController,
  createCategoriaController,
  updateCategoriaController,
  deleteCategoriaController,
};
