import { Response, Request } from 'express';
import bcrypt from 'bcryptjs';
import User from '../model/user.model';

const createUser = async (req: Request, res: Response) => {
  const { password } = req.body;

  try {

    const userModel = new User(req.body);

    // Encriptar contraseña
    const salt = bcrypt.genSaltSync();
    userModel.password = bcrypt.hashSync(password, salt);

    // Guardar Usuario
    await userModel.save();

    res.status(200).json({
      ok: true,
      usuario: userModel,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: 'Error del servidor al tratar de crear el usuario',
      error
    });
  }
};

const getUser = async (req: Request, res: Response) => {
  const id = req.params.id;
  try {
    const user = await User.findById(id);

    res.status(200).json({
      ok: true,
      user,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: 'Error al trarar de recuperar el usuario',
    });
  }
};

const getAllUser = async (req: Request, res: Response) => {
  try {
    const users = await User.find({});
    res.status(200).json({
      ok: true,
      usuarios: users,
    });
  } catch (error) {}
};

export { createUser, getUser, getAllUser };
