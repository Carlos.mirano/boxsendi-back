import { Request, Response } from 'express';
import scrap from '../helper/scrap';
import Product from '../model/product.model';

const scraping = async (req: Request, res: Response) => {
  const { url } = req.body;

  try {
    const item = {
      descripcion: '',
      moneda: '',
      precio: '',
      imagen: '',
      asin: '',
    };

    const s = await scrap(url);
    item.descripcion = s?.descripcion as string;
    item.moneda = s?.moneda as string;
    item.precio = s?.precio as string;
    item.imagen = s?.imagen as string;
    item.asin = s?.asin as string;

    const { asin } = item;

    const existeItem = await Product.findOne({ asin });
    if (!existeItem) {
      const items = new Product(item);
      await items.save();
    }
    return res.status(200).json({
      ok: true,
      product: item,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      msg: 'Error al requistrar el item',
    });
  }
};

export { scraping };
