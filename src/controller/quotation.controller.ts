import { Response, Request, NextFunction } from 'express';
import Quotation from '../model/quotation.model';
import {
  deleteQuotationService,
  getAllQuotationService,
  updateQuotationService,
} from '../service/quotation.service';

const createQuotation = async (req: Request, res: Response) => {
  try {
    const quotationModel = new Quotation(req.body);
    await quotationModel.save();
    res.status(200).json({
      ok: true,
      cotizacion: quotationModel,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: 'Error del servidor al tratar de crear la cotización',
    });
  }
};

const getQuotations = async (req: Request, res: Response) => {
  const id = req.params.id;
  try {
    const quotation = await Quotation.find({ usuario: id }).populate(
      'producto'
    );
    res.status(200).json({
      ok: true,
      cotizaciones: quotation,
    });
  } catch (error) {
    res.status(500).json({
      ok: true,
      msg: 'Error al tratar de recuperar la lista de cotizaciones',
    });
  }
};

// const updateQuotationController = async (req: Request, res: Response) => {
//   const id = req.params.id;
//   try {
//     const db = await updateQuotationService(id, req.body);
//     return res.status(200).json({
//       db,
//       msg: 'Actualización Exitosa',
//     });
//   } catch (error) {
//     return res.status(500).json({
//       error,
//       msg: 'Error al actualizar la orden',
//     });
//   }
// };

// amc
const updateQuotationController = async (req: Request, res: Response, next: NextFunction) => {
  const id = req.params.id;
  try {
    const db = await updateQuotationService(id, req.body);

    (<any>req).db = (<any>db);
    next();
    /* return res.status(200).json({
      db,
      msg: 'Actualización Exitosa',
    }); */
  } catch (error) {
    return res.status(500).json({
      error,
      msg: 'Error al actualizar la orden',
    });
  }
};

const deleteQuotationController = async (req: Request, res: Response) => {
  const id = req.params.id;
  const estado = req.params.estado;
  const { quotation, msg } = await deleteQuotationService(id, estado);
  res.status(200).json({
    quotation,
    msg,
  });
};

const getAllQuotationController = async (req: Request, res: Response) => {
  const { quotation, msg } = await getAllQuotationService();
  res.status(200).json({
    quotation,
    msg,
  });
};

export {
  createQuotation,
  getQuotations,
  updateQuotationController,
  deleteQuotationController,
  getAllQuotationController,
};
