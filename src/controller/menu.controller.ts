import { Request, Response } from 'express';
import { IMenu } from '../interface/menu.interface';
import {
  listMenuService,
  createMenuService,
  updateMenuService,
  deleteMenuService,
} from '../service/menu.service';

const listMenuController = async (req: Request, res: Response) => {
  const id = req.params.id;
  const { menus, msg } = await listMenuService(id);
  return res.status(200).json({
    menus,
    msg,
  });
};

const createMenuController = async (req: Request, res: Response) => {
  const menub: IMenu = <any>req.body;
  const { menu, msg } = await createMenuService(menub);
  return res.status(200).json({
    menu,
    msg,
  });
};

const updateMenuController = async (req: Request, res: Response) => {
  const id = req.params.id;
  const menub: IMenu = <any>req.body;
  const { menu, msg } = await updateMenuService(id, menub);
  res.status(200).json({
    menu,
    msg,
  });
};

const deleteMenuController = async (req: Request, res: Response) => {
  const id = req.params.id;
  const { msg } = await deleteMenuService(id);
  return res.status(200).json({
    msg,
  });
};

export {
  listMenuController,
  createMenuController,
  updateMenuController,
  deleteMenuController,
};
