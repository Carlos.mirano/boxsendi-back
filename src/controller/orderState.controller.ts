import { Request, Response } from 'express';
import { IOrderState } from '../interface/orderState.interface';
import {
  listOrderStateService,
  createOrderStateService,
  updateOrderStateService,
  deleteOrderStateSerice,
} from '../service/orderState.service';

const listOrderStateController = async (req: Request, res: Response) => {
  const orderState = await listOrderStateService();
  return res.status(200).json({
    orderState,
  });
};

const createOrderStateController = async (req: Request, res: Response) => {
  const orderStateBody: IOrderState = <any>req.body;
  const db = await createOrderStateService(orderStateBody);
  return res.status(200).json({
    db,
  });
};

const updateOrderStateController = async (req: Request, res: Response) => {
  const id = req.params.id;
  const orderStateBody: IOrderState = <any>req.body;
  const db = await updateOrderStateService(id, orderStateBody);
  return res.status(200).json({
    db,
  });
};

const deleteOrderStateController = async (req: Request, res: Response) => {
  const id = req.params.id;
  const db = await deleteOrderStateSerice(id);
  return res.status(200).json({
    db,
  });
};

export {
  listOrderStateController,
  createOrderStateController,
  updateOrderStateController,
  deleteOrderStateController,
};
