import { Request, Response } from 'express';
import bcrypt from 'bcryptjs';

import User from '../model/user.model';
import JWT from '../helper/jwt';
import { menuAdmin, menuUser } from '../utils/menu';
import Menu from '../model/menu.model';
import { NO_ENCONTRADO } from '../utils/codes';

const login = async (req: Request, res: Response) => {
  const { mail, password } = req.body;
  let menu;

  try {
    const userDB: any = await User.findOne({ mail });
    if (!userDB) {
      return res.status(404).json({
        ok: false,
        msg: 'Usuario no encontrado',
        code: NO_ENCONTRADO,
      });
    }

    const validPass = bcrypt.compareSync(password, userDB.password);

    if (!validPass) {
      return res.status(404).json({
        ok: false,
        msg: 'Usuario no encontrado',
        code: NO_ENCONTRADO,
      });
    }

    // Generar el token
    const token: any = await JWT(userDB.id);

    if (userDB.admin) {
      menu = await Menu.find({ admin: true });
    } else {
      menu = await Menu.find({ admin: false });
    }

    res.status(200).json({
      ok: true,
      userId: userDB.id,
      token,
      user: userDB,
      menu,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: 'Error del servidor al tratar de Iniciar Sesión',
    });
  }
};

export default login;
