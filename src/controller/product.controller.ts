import { Request, Response } from 'express';
import { Connect, Query } from '../database/mysql';
import Product from '../model/product.model';

const getAllProducts = async (
  req: Request,
  res: Response
): Promise<Response | void> => {
  let query = 'SELECT * FROM bh_product';
  Connect()
    .then((connection) => {
      Query(connection, query)
        .then((result) => {
          return res.status(200).json({
            result,
          });
        })
        .catch((error) => {
          console.log('Error al traer los productos');
          return res.status(200).json({
            message: error.message,
            error,
          });
        })
        .finally(() => {
          console.log('Cerrando conexión a la db');
          connection.end();
        });
    })
    .catch((error) => {
      console.log('Error al conectar a la base de datos', error.message);
      return res.status(200).json({
        message: error.message,
        error,
      });
    });
};

const getProduct = async (req: Request, res: Response) => {
  const asin = req.params.asin;
  try {
    const product = await Product.find({ asin: asin });
    res.status(200).json({
      ok: true,
      producto: product,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: 'Error al recuperar el producto',
    });
  }
};

export { getAllProducts, getProduct };
