const menuUser = [
  {
    icon: 'mdi-apps',
    title: 'Inicio',
    to: '/inicio',
  },
  {
    icon: 'mdi-clipboard-text-outline',
    title: 'Mis Ordenes',
    to: '/inicio/misordenes',
  },
  {
    icon: 'mdi-cart-outline',
    title: 'Order de Compra',
    to: '/inicio/orden',
  },
];

const menuAdmin = [
  {
    icon: 'mdi-apps',
    title: 'Inicio',
    to: '/inicio',
  },
  {
    icon: 'mdi-apps',
    title: 'Usuarios',
    to: '/inicio/usuarios',
  },
  {
    icon: 'mdi-apps',
    title: 'Ordenes',
    to: '/inicio/ordenes',
  },
  {
    icon: 'mdi-apps',
    title: 'Productos',
    to: '/inicio/productos',
  },
];

export { menuUser, menuAdmin };
