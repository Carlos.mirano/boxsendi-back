import { App } from './app';
import { run } from './database/mongo';

function main() {
  run();
  const app = new App();
  app.listen();
}

main();
