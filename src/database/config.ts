import dotenv from 'dotenv';

dotenv.config();

const MYSQL_HOST = process.env.MYSQL_HOST as string;
const MYSQL_DATABASE = process.env.MYSQL_DATABASE as string;
const MYSQL_USER = process.env.MYSQL_USER as string;
const MYSQL_PASS = process.env.MYSQL_PASS as string;

const MYSQL = {
  host: MYSQL_HOST,
  database: MYSQL_DATABASE,
  user: MYSQL_USER,
  pass: MYSQL_PASS,
};

const config = {
  mysql: MYSQL,
};

export default config;
