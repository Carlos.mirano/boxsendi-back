import dotenv from 'dotenv';
import { Pool } from 'pg';

dotenv.config();
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const pool = new Pool({
  user: process.env.PG_USER as string,
  host: process.env.PG_HOST as string,
  password: process.env.PG_PASS as string,
  database: process.env.PG_DATABASE as string,
  port: 5432,
  ssl: true,
  max: 30,
});

export { pool };
