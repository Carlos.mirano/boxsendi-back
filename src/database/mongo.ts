import { connect } from 'mongoose';

export async function run(): Promise<void> {
  await connect(process.env.URL_MONGO as string, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
    .then(() => {
      console.log('Base de datos conectada');
    })
    .catch((err) => {
      console.log('Error al conectar a la base de datos', err);
    });
}
