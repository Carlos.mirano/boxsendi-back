import express, { Application } from 'express';
import morgan from 'morgan';
import indexRoutes from './routes/index.routes';
import cors from 'cors';
import helmet from "helmet";

import productRoutes from './routes/product.routes';
import userRoutes from './routes/user.routes';
import authRoutes from './routes/auth.routes';
import scrapRoutes from './routes/scrap.routes';
import quotationRoutes from './routes/quotation.routes';
import menuRoutes from './routes/menu.routes';
import categoriaRoutes from './routes/categoria.routes';
import orderStateRoutes from './routes/orderState.routes';

export class App {
  private app: Application;

  constructor(private port?: number) {
    this.app = express();
    this.settings();
    this.middlewares();
    this.routes();
  }

  settings() {
    this.app.set('port', this.port || process.env.PORT || 3000);
  }

  middlewares() {
    this.app.use(morgan('dev'));
    this.app.use(express.json());
    this.app.use(cors());
    // this.app.use(helmet())
  }

  routes() {
    this.app.use(indexRoutes);
    this.app.use('/api/productos', productRoutes);
    this.app.use('/api/usuarios', userRoutes);
    this.app.use('/auth/login', authRoutes);
    this.app.use('/api/scraping', scrapRoutes);
    this.app.use('/api/cotizaciones', quotationRoutes);
    this.app.use('/api/menu', menuRoutes);
    this.app.use('/api/categoria', categoriaRoutes);
    this.app.use('/api/orderstate', orderStateRoutes);
  }

  async listen(): Promise<void> {
    await this.app.listen(this.app.get('port'));
    console.log('Servidor ejecutando en el puerto:', this.app.get('port'));
  }
}
