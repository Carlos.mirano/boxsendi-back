import { Schema, model } from 'mongoose';
import { IUser } from '../interface/user.interface';

const userSchema = new Schema<IUser>({
  name: { type: String, require: true },
  user: { type: String, require: true, unique: true },
  mail: { type: String, require: true, unique: true },
  password: { type: String, require: true },
  role: { type: String, default: 'ROLE_USER' },
  admin: { type: Boolean, default: false },
});

userSchema.method('toJSON', function () {
  const { __v, _id, password, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const User = model<IUser>('User', userSchema);

export default User;
