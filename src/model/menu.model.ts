import { Schema, model } from 'mongoose';
import { IMenu } from '../interface/menu.interface';

const menuSchema = new Schema<IMenu>(
  {
    admin: { type: Boolean, require: true },
    icon: { type: String, require: true },
    title: { type: String, require: true },
    to: { type: String, require: true },
  },
  {
    timestamps: true,
  }
);

menuSchema.method('toJSON', function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const Menu = model<IMenu>('Menu', menuSchema);

export default Menu;
