import { Schema, model } from 'mongoose';
import { IQuotation } from '../interface/quotation.interface';

const quotationSchema = new Schema<IQuotation>(
  {
    usuario: { type: Schema.Types.ObjectId, ref: 'User' },
    asin: { type: String },
    cantidad: { type: String },
    unidad: { type: String },
    peso: { type: String },
    paquete: { type: String },
    categoria: { type: String },
    precioTotal: { type: String },
    flete: { type: String },
    arancel: { type: String },
    subTotal: { type: String },
    descuento: { type: String },
    impuesto: { type: String },
    total: { type: String },
    origen: { type: String },
    destino: { type: String },
    estado: { type: String },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

quotationSchema.virtual('producto', {
  ref: 'Product',
  localField: 'asin',
  foreignField: 'asin',
});

quotationSchema.method('toJSON', function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const Quotation = model<IQuotation>('Quotation', quotationSchema);

export default Quotation;
