import { Schema, model } from 'mongoose';
import { IOrderState } from '../interface/orderState.interface';

const orderStateSchema = new Schema<IOrderState>(
  {
    idState: { type: Number, require: true, unique: true },
    description: { type: String, require: true },
  },
  {
    timestamps: true,
  }
);

orderStateSchema.method('toJSON', function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const OrderState = model<IOrderState>('OrderState', orderStateSchema);

export default OrderState;
