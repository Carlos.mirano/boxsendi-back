import { Schema, model } from 'mongoose';
import { IProducto } from '../interface/product.interface';

const productSchema = new Schema<IProducto>({
  asin: { type: String, unique: true },
  imagen: { type: String },
  descripcion: { type: String },
  moneda: { type: String },
  precio: { type: Number },
});

productSchema.method('toJSON', function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const Product = model<IProducto>('Product', productSchema);

export default Product;
