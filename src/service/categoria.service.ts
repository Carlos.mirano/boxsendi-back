import { pool } from '../database/postgresql';

import { ICategoria } from '../interface/categoria.interface';

const listCategoriaService = async () => {
  pool.connect();
  try {
    const categorias = await pool
      .query('SELECT * FROM boxsendi.categoria')
      .then((data) => data.rows);
    return { categorias };
  } catch (error) {
    return { error };
  }
};

const createCategoriaService = async (categoria: ICategoria) => {
  pool.connect();
  const save = await pool.query(
    'INSERT INTO boxsendi.categoria(key, name, desct) VALUES($1, $2, $3);',
    [categoria.key, categoria.name, categoria.desct]
  );
  return save;
};

const updateCategoriaService = async (categoria: ICategoria) => {
  try {
    (await pool.connect()).query(
      'UPDATE boxsendi.categoria SET name = $1, desct = $2 WHERE key = $3',
      [categoria.name, categoria.desct, categoria.key]
    );
  } catch (error) {
    return error;
  }
};

const deleteCategoriaService = async (categoria: ICategoria) => {
  try {
    (await pool.connect()).query(
      'DELETE FROM boxsendi.categoria WHERE key = $1',
      [categoria.key]
    );
  } catch (error) {
    return error;
  }
};

export {
  listCategoriaService,
  createCategoriaService,
  updateCategoriaService,
  deleteCategoriaService,
};
