import { IQuotation } from '../interface/quotation.interface';
import Quotation from '../model/quotation.model';

const deleteQuotationService = async (id: string, estado: any) => {
  const quotationdb = await Quotation.findById(id);
  if (!quotationdb) {
    return { msg: 'Delete: La Cotización no existe' };
  }
  const deleteQuotation = await Quotation.updateOne(
    { _id: id },
    { $set: { estado: estado } }
  );
  return { quotation: deleteQuotation, msg: 'Delete: Cotización cancelada' };
};

const getAllQuotationService = async () => {
  try {
    const quotationdb = await Quotation.find({}).populate('producto');
    return { quotation: quotationdb, msg: 'List: Consulta Exitosa' };
  } catch (error) {
    return { msg: 'Error al recuperar la lista de ordenes' };
  }
};

// const updateQuotationService = async (id: string, quotation: any) => {
//   const db = await Quotation.findByIdAndUpdate(id, quotation);
//   return db;
// };
// amc
const updateQuotationService = async (id: string, quotation: any) => {
  const db = await Quotation.findByIdAndUpdate(id, quotation, { new : true });
  return db;
};

export {
  deleteQuotationService,
  getAllQuotationService,
  updateQuotationService,
};
