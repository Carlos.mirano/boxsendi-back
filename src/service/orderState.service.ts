import { IOrderState } from '../interface/orderState.interface';
import OrderState from '../model/orderState.model';

const listOrderStateService = async () => {
  const orderState = await OrderState.find({});
  return orderState;
};

const createOrderStateService = async (orderState: IOrderState) => {
  const createOrder = new OrderState(orderState);
  const orderstatedb = await createOrder.save();
  return orderstatedb;
};

const updateOrderStateService = async (id: string, orderState: IOrderState) => {
  const updateOrderState = await OrderState.findByIdAndUpdate(id, orderState, {
    new: true,
  });
  return updateOrderState;
};

const deleteOrderStateSerice = async (id: string) => {
  const db = await OrderState.deleteOne({_id: id});
  return db;
};

export {
  listOrderStateService,
  createOrderStateService,
  updateOrderStateService,
  deleteOrderStateSerice,
};
