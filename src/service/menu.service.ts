import { IMenu } from '../interface/menu.interface';
import Menu from '../model/menu.model';
import User from '../model/user.model';

const listMenuService = async (id: string) => {
  const user: any = await User.findById(id);
  if (user.admin) {
    const listMenuAdmin = await Menu.find({ admin: true });
    return { menus: listMenuAdmin, msg: 'List: Consulta exitosa' };
  } else {
    const listMenuUser = await Menu.find({ admin: false });
    return { menus: listMenuUser, msg: 'List: Consulta exitosa' };
  }
};
const createMenuService = async (menu: IMenu) => {
  const createMenu = new Menu(menu);
  const menudb = await createMenu.save();
  return { menu: menudb, msg: 'Create: Menú creado con exito' };
};
const updateMenuService = async (id: string, menu: IMenu) => {
  const menudb = await Menu.findById(id);
  if (!menudb) {
    return { msg: 'Update: El menú no existe' };
  }
  const updateMenu = await Menu.findByIdAndUpdate(id, menu, { new: true });
  return { menu: updateMenu, msg: 'Update: Actualización exitosa' };
};
const deleteMenuService = async (id: string) => {
  const menudb = await Menu.findById(id);
  if (!menudb) {
    return { msg: 'Delete: El menú no existe' };
  }
  await Menu.findByIdAndDelete(id);
  return { msg: 'Delete: Eliminación exitosa' };
};

export {
  listMenuService,
  createMenuService,
  updateMenuService,
  deleteMenuService,
};
